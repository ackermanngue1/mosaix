#include "helper/helper.h"
#include "image/image.h"
#include <chrono>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <dirent.h>
#include <png++/png.hpp>
#include <string>

using namespace std;
using namespace chrono;

int SCALE_FACTOR = 2;

const int MIN_WIDTH_TO_UPSCALE = 2560;
const int MIN_HEIGHT_TO_UPSCALE = 1440;

const string OUTPUT_FOLDER_PATH = "../output/";
string TEXTURE_FOLDER_PATH;
const string IMAGE_PIXELATED_SUFFIX = "_pixelated.png";
const string IMAGE_PIXELATED_BLOCK_USED_SUFFIX = "_blocks.txt";

int main(int argc, char **argv) {
  if (argc < 8) {
    printUsage();
    return 1;
  }

  char *inputFilename = argv[1];
  const char *filename = extractFilename(inputFilename);
  char *textureFolder = argv[2];
  textureFolder = appendSlashIfNeeded(textureFolder);
  int blocksPerRow = atoi(argv[3]);
  blocksPerRow == 0 ? blocksPerRow = 1 : atoi(argv[3]);
  int numThreads = atoi(argv[4]);
  numThreads == 0 ? numThreads = 1 : atoi(argv[4]);
  bool squareTiling = atoi(argv[5]);
  bool outputUsage = atoi(argv[6]);
  double SCALE_FACTOR = atof(argv[7]);

  if (!isPNG(inputFilename)) {
    convertToPNG(inputFilename);
    inputFilename = strcat(inputFilename, ".png");
  }

  int textureWidth, textureHeight;
  auto imageMap = loadImagesToMap(textureFolder, &textureWidth, &textureHeight);

  png::image<png::rgb_pixel> inputImage(inputFilename);

  int blockWidth =
      static_cast<int>(ceil(inputImage.get_width() / blocksPerRow));
  if (blockWidth % 2 != 0)
    blockWidth++;
  int blockHeight =
      static_cast<int>(ceil(inputImage.get_height() / blocksPerRow));
  if (blockHeight % 2 != 0)
    blockHeight++;

  int outputWidth = blocksPerRow * blockWidth;
  int outputHeight = blocksPerRow * blockHeight;

  if (squareTiling)
    blockHeight = blockWidth;

  printf("================================================================="
         "==\n");
  printf("Image : %s\n", filename);
  printf("Scale factor  %lf\n", SCALE_FACTOR);
  printf("Input size (w: %d, h: %d)\t", inputImage.get_width(),
         inputImage.get_height());

  if (outputWidth <= MIN_WIDTH_TO_UPSCALE ||
      outputHeight <= MIN_HEIGHT_TO_UPSCALE) {
    blockWidth *= SCALE_FACTOR;
    blockHeight *= SCALE_FACTOR;
    outputWidth *= SCALE_FACTOR;
    outputHeight *= SCALE_FACTOR;
    inputImage = stretchUp(inputImage, outputWidth, outputHeight);
  }

  printf("Output size (w: %d, h: %d)\n", outputWidth, outputHeight);
  printf("Texture size is (w: %d, h: %d)\t", textureWidth, textureHeight);
  printf("Block size is (w: %d, h: %d)\n", blockWidth, blockHeight);
  printf(
      "===================================================================\n");

  auto startPixelating = high_resolution_clock::now();

  map<string, int> imageUsageCount;
  png::image<png::rgb_pixel> outputImage = pixelateImageMultithreaded(
      filename, imageMap, inputImage, blockWidth, blockHeight, outputWidth,
      outputHeight, textureFolder, numThreads, &imageUsageCount);

  auto endPixelating = high_resolution_clock::now();
  printf("\n");
  auto duration =
      duration_cast<std::chrono::milliseconds>(endPixelating - startPixelating);
  printf("Image pixelation and mosaic placement took %s\n",
         formatDuration(duration).c_str());

  printf("Saving the result.....\n");
  auto startWritingImage = high_resolution_clock::now();
  createDirectory(OUTPUT_FOLDER_PATH);
  // Save the pixelated image to a file
  string outputFilename =
      OUTPUT_FOLDER_PATH + string(filename) + IMAGE_PIXELATED_SUFFIX;
  outputImage.write(outputFilename.c_str());
  auto endWritingImage = high_resolution_clock::now();
  printf("\n");
  duration = duration_cast<std::chrono::milliseconds>(endWritingImage -
                                                      startWritingImage);
  printf("Writing the image took %s\n", formatDuration(duration).c_str());

  if (outputUsage) {
    auto startOutputUsage = high_resolution_clock::now();
    // Redirect the cout to a file
    string usageFilename = OUTPUT_FOLDER_PATH + string(filename) +
                           IMAGE_PIXELATED_BLOCK_USED_SUFFIX;
    std::ofstream usageFile(usageFilename);

    // Write usage statistics to the file stream
    usageFile << "Images used:" << endl;
    for (const auto &pair : imageUsageCount) {
      if (pair.second > 0)
        usageFile << pair.first << " is used " << pair.second << " times"
                  << endl;
    }

    // Close the file stream
    usageFile.close();
    auto endoutputUsage = high_resolution_clock::now();
    printf("\n");
    duration = duration_cast<std::chrono::milliseconds>(endoutputUsage -
                                                        startOutputUsage);
    printf("Writing the images needed to make the mosaic took %s\n",
           formatDuration(duration).c_str());
  }

  printOutputMessage(filename, outputUsage);
  return 0;
}
