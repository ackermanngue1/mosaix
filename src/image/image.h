#ifndef IMAGE_H
#define IMAGE_H

#include <map>
#include <png++/png.hpp>
#include <vector>

using namespace std;

struct RGBPixelComparator {
  bool operator()(const png::rgb_pixel &lhs, const png::rgb_pixel &rhs) const;
};

/**
 * Calculates the mean color of an image.
 * @param image The image for which to calculate the mean color.
 * @return The mean color of the image.
 */
png::rgb_pixel calculateMeanColor(const png::image<png::rgb_pixel> &image);

/**
 * Loads images from a folder into a map, using the mean color of each image as
 * the key.
 * @param folderPath The path to the folder containing the images.
 * @return A map associating mean colors with lists of filenames.
 */
std::map<png::rgb_pixel, std::vector<std::string>, RGBPixelComparator>
loadImagesToMap(const std::string &folderPath, int *textureWidth,
                int *textureHeight);

/**
 * Calculates the Euclidean distance between two colors.
 * @param color1 The first color.
 * @param color2 The second color.
 * @return The Euclidean distance between the two colors.
 */
double calculateDistance(const png::rgb_pixel &color1,
                         const png::rgb_pixel &color2);

/**
 * Finds the closest image in the image map to the target color.
 * @param targetColor The target color to find the closest image for.
 * @param imageMap The map of mean colors to image filenames.
 * @return The filename of the closest image.
 */
std::string findClosestImage(
    const png::rgb_pixel &targetColor,
    const std::map<png::rgb_pixel, std::vector<std::string>, RGBPixelComparator>
        &imageMap);

/**
 * Stretches down an image to a new height while preserving aspect ratio.
 * @param inputImage The input image to stretch down.
 * @param newHeight The new height for the stretched image.
 * @return The stretched image.
 */
png::image<png::rgb_pixel>
stretchDown(const png::image<png::rgb_pixel> &inputImage, int newWidth,
            int newHeight);

png::image<png::rgb_pixel>
stretchUp(const png::image<png::rgb_pixel> &inputImage, int newWidth,
          int newHeight);

png::image<png::rgb_pixel>
flip90DegreesClockwise(png::image<png::rgb_pixel> inputImage);

/**
 * Pixelates an image using a map of images and a given texture folder.
 * @param filename The filename of the input image.
 * @param imageMap The map of mean colors to image filenames.
 * @param inputImage The input image to pixelate.
 * @param pixelSize The size of each pixel block in the output image.
 * @param outputSize The size of the output image.
 * @param textureFolder The path to the folder containing texture images.
 */
void pixelateImage(const char *filename,
                   const std::map<png::rgb_pixel, std::vector<std::string>,
                                  RGBPixelComparator> &imageMap,
                   png::image<png::rgb_pixel> &inputImage, int pixelSize,
                   int outputSize, const char *textureFolder);

/**
 * Calculates the output size for pixelation based on input dimensions and
 * desired output size.
 * @param inputWidth The width of the input image.
 * @param inputHeight The height of the input image.
 * @param outputSize The desired size of the output image.
 * @param outputWidth A reference to store the calculated output width.
 * @param outputHeight A reference to store the calculated output height.
 */
void calculateOutputSize(int inputWidth, int inputHeight, int outputSize,
                         int *outputWidth, int *outputHeight);

void pixelateBlockRange(
    const char *filename,
    const map<png::rgb_pixel, vector<string>, RGBPixelComparator> &imageMap,
    png::image<png::rgb_pixel> &inputImage,
    png::image<png::rgb_pixel> &outputImage, int pixelWidth, int pixelHeight,
    int outputWidth, int outputHeight, int startY, int endY, int startX,
    int endX, const char *textureFolder, int threadIndex);

png::image<png::rgb_pixel> pixelateImageMultithreaded(
    const char *filename,
    const map<png::rgb_pixel, vector<string>, RGBPixelComparator> &imageMap,
    png::image<png::rgb_pixel> &inputImage, int pixelWidth, int pixelHeight,
    int outputWidth, int outputHeight, const char *textureFolder,
    int numThreads, map<string, int> *usageCount);

#endif // IMAGE_H
