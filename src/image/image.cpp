#include "image.h"
#include "../helper/helper.h"
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

const string FILE_EXTENSION = ".png";

map<string, int> imageUsageCount;

bool RGBPixelComparator::operator()(const png::rgb_pixel &lhs,
                                    const png::rgb_pixel &rhs) const {
  // Compare pixels based on grayscale value
  int lhsGray = lhs.red + lhs.green + lhs.blue;
  int rhsGray = rhs.red + rhs.green + rhs.blue;
  return lhsGray < rhsGray;
}

png::rgb_pixel calculateMeanColor(const png::image<png::rgb_pixel> &image) {
  int totalRed = 0, totalGreen = 0, totalBlue = 0;
  int numPixels = image.get_width() * image.get_height();

  for (size_t y = 0; y < image.get_height(); y++) {
    for (size_t x = 0; x < image.get_width(); x++) {
      totalRed += image[y][x].red;
      totalGreen += image[y][x].green;
      totalBlue += image[y][x].blue;
    }
  }

  int resultRed, resultGreen, resultBlue;

  if (totalRed == 0 && totalGreen == 0 && totalBlue == 0 && numPixels == 0)
    return png::rgb_pixel(0, 0, 0);

  return png::rgb_pixel(totalRed / numPixels, totalGreen / numPixels,
                        totalBlue / numPixels);
}

std::map<png::rgb_pixel, std::vector<std::string>, RGBPixelComparator>
loadImagesToMap(const std::string &folderPath, int *textureWidth,
                int *textureHeight) {
  std::map<png::rgb_pixel, std::vector<std::string>, RGBPixelComparator>
      imageMap;
  std::vector<std::string> imageFiles = getFilesInFolder(folderPath);

  for (const std::string &filename : imageFiles) {
    std::string imagePath = folderPath + filename;

    // Check if the file is a PNG image
    if (!isPNG(filename)) {
      convertToPNG(filename.c_str());
      imagePath = filename + FILE_EXTENSION;
    }
    try {
      png::image<png::rgb_pixel> image(imagePath);
      *textureWidth = image.get_width();
      *textureHeight = image.get_height();
      png::rgb_pixel meanColor = calculateMeanColor(image);
      imageMap[meanColor].push_back(filename);
      imageUsageCount[filename] = 0;
    } catch (const png::std_error &error) {
      std::cerr << "Error loading image: " << imagePath << " - " << error.what()
                << std::endl;
    }
  }

  return imageMap;
}

png::image<png::rgb_pixel>
flip90DegreesClockwise(png::image<png::rgb_pixel> inputImage) {

  // Get the dimensions of the input image
  size_t width = inputImage.get_width();
  size_t height = inputImage.get_height();

  // Create an empty image for the flipped result
  png::image<png::rgb_pixel> flippedImage(height, width);

  // Flip the image by 90 degrees clockwise
  for (size_t y = 0; y < height; y++) {
    for (size_t x = 0; x < width; x++) {
      flippedImage[x][height - y - 1] = inputImage[y][x];
    }
  }
  return flippedImage;
}

double calculateDistance(const png::rgb_pixel &color1,
                         const png::rgb_pixel &color2) {
  int diffRed = color1.red - color2.red;
  int diffGreen = color1.green - color2.green;
  int diffBlue = color1.blue - color2.blue;
  return sqrt(diffRed * diffRed + diffGreen * diffGreen + diffBlue * diffBlue);
}

std::string findClosestImage(
    const png::rgb_pixel &targetColor,
    const std::map<png::rgb_pixel, std::vector<std::string>, RGBPixelComparator>
        &imageMap) {
  double minDistance = std::numeric_limits<double>::max();
  std::string closestImage = "";
  for (const auto &pair : imageMap) {
    double distance = calculateDistance(targetColor, pair.first);
    if (distance <= minDistance) {
      minDistance = distance;
      closestImage = pair.second.front();
    }
  }
  imageUsageCount[closestImage]++;

  return closestImage;
}

png::image<png::rgb_pixel>
stretchDown(const png::image<png::rgb_pixel> &inputImage, int newWidth,
            int newHeight) {
  int inputWidth = inputImage.get_width();
  int inputHeight = inputImage.get_height();

  double xScaleFactor = static_cast<double>(inputWidth - 1) / (newWidth - 1);
  double yScaleFactor = static_cast<double>(inputHeight - 1) / (newHeight - 1);

  png::image<png::rgb_pixel> outputImage(newWidth, newHeight);

  for (int y = 0; y < newHeight; y++) {
    for (int x = 0; x < newWidth; x++) {
      int startX = static_cast<int>(x * xScaleFactor);
      int endX = static_cast<int>((x + 1) * xScaleFactor);
      int startY = static_cast<int>(y * yScaleFactor);
      int endY = static_cast<int>((y + 1) * yScaleFactor);

      // Adjust range if it exceeds input dimensions
      if (endX >= inputWidth)
        endX = inputWidth - 1;
      if (endY >= inputHeight)
        endY = inputHeight - 1;

      int totalR = 0, totalG = 0, totalB = 0;
      int pixelCount = 0;

      for (int j = startY; j <= endY; j++) {
        for (int i = startX; i <= endX; i++) {
          totalR += inputImage[j][i].red;
          totalG += inputImage[j][i].green;
          totalB += inputImage[j][i].blue;
          pixelCount++;
        }
      }

      // Ensure at least one pixel was considered
      if (pixelCount > 0) {
        outputImage[y][x].red = totalR / pixelCount;
        outputImage[y][x].green = totalG / pixelCount;
        outputImage[y][x].blue = totalB / pixelCount;
      }
    }
  }

  return outputImage;
}

png::image<png::rgb_pixel>
stretchUp(const png::image<png::rgb_pixel> &inputImage, int newWidth,
          int newHeight) {
  int inputWidth = inputImage.get_width();
  int inputHeight = inputImage.get_height();

  double xScaleFactor = static_cast<double>(inputWidth) / newWidth;
  double yScaleFactor = static_cast<double>(inputHeight) / newHeight;

  png::image<png::rgb_pixel> outputImage(newWidth, newHeight);

  for (int y = 0; y < newHeight; ++y) {
    for (int x = 0; x < newWidth; ++x) {
      double origX = static_cast<double>(x) * xScaleFactor;
      double origY = static_cast<double>(y) * yScaleFactor;

      int startX = static_cast<int>(origX);
      int startY = static_cast<int>(origY);
      int endX = std::min(startX + 1, inputWidth - 1);
      int endY = std::min(startY + 1, inputHeight - 1);

      double xWeight = origX - startX;
      double yWeight = origY - startY;

      // Interpolate pixel values
      png::rgb_pixel interpolatedPixel;
      interpolatedPixel.red = static_cast<uint8_t>(
          (1 - yWeight) * ((1 - xWeight) * inputImage[startY][startX].red +
                           xWeight * inputImage[startY][endX].red) +
          yWeight * ((1 - xWeight) * inputImage[endY][startX].red +
                     xWeight * inputImage[endY][endX].red));
      interpolatedPixel.green = static_cast<uint8_t>(
          (1 - yWeight) * ((1 - xWeight) * inputImage[startY][startX].green +
                           xWeight * inputImage[startY][endX].green) +
          yWeight * ((1 - xWeight) * inputImage[endY][startX].green +
                     xWeight * inputImage[endY][endX].green));
      interpolatedPixel.blue = static_cast<uint8_t>(
          (1 - yWeight) * ((1 - xWeight) * inputImage[startY][startX].blue +
                           xWeight * inputImage[startY][endX].blue) +
          yWeight * ((1 - xWeight) * inputImage[endY][startX].blue +
                     xWeight * inputImage[endY][endX].blue));

      outputImage[y][x] = interpolatedPixel;
    }
  }

  return outputImage;
}

// Function to compute the output size
void calculateOutputSize(int inputWidth, int inputHeight, int outputSize,
                         int *outputWidth, int *outputHeight) {
  double aspectRatio = (double)inputWidth / inputHeight;

  if (inputWidth > inputHeight) {
    *outputWidth = outputSize;
    *outputHeight = static_cast<int>(outputSize / aspectRatio);
  } else {
    *outputWidth = static_cast<int>(outputSize * aspectRatio);
    *outputHeight = outputSize;
  }
}

void pixelateBlockRange(
    const char *filename,
    const map<png::rgb_pixel, vector<string>, RGBPixelComparator> &imageMap,
    png::image<png::rgb_pixel> &inputImage,
    png::image<png::rgb_pixel> &outputImage, int pixelWidth, int pixelHeight,
    int outputWidth, int outputHeight, int startY, int endY, int startX,
    int endX, const char *textureFolder, int threadIndex) {

  // Get image dimensions
  int inputWidth = inputImage.get_width();
  int inputHeight = inputImage.get_height();

  // Ensure that the block does not exceed the image boundaries
  int blockEndX = std::min(endX, outputWidth);
  int blockEndY = std::min(endY, outputHeight);

  printf("Thread %d is computing data from (x: %d, y: %d) to (x: %d, y: %d)\n",
         threadIndex + 1, startX, startY, endX, endY);

  int totalIterations =
      static_cast<int>(ceil((double)(endY - startY) / pixelHeight)) *
      static_cast<int>(ceil((double)(endX - startX) / pixelWidth));
  int currentIteration = 0;

  for (int y = startY; y < endY; y += pixelHeight) {
    for (int x = startX; x < endX; x += pixelWidth) {
      int subImageWidth = std::min(pixelWidth, endX - x);
      int subImageHeight = std::min(pixelHeight, endY - y);

      // Extract the sub-image from the input image
      png::image<png::rgb_pixel> subImage(subImageWidth, subImageHeight);
      for (int j = 0; j < subImageHeight; j++) {
        // Ensure that the vertical coordinate (y + j) is within the bounds of
        // the input image
        if (y + j < inputHeight) {
          for (int i = 0; i < subImageWidth; i++) {
            // Ensure that the horizontal coordinate (x + i) is within the
            // bounds of the input image
            if (x + i < inputWidth) {
              subImage[j][i] = inputImage[y + j][x + i];
            } else {
              // Handle the case where the horizontal coordinate is out of
              // bounds For example, assign a default value
              subImage[j][i] = inputImage[j][i];
            }
          }
        } else {
          // Handle the case where the vertical coordinate is out of bounds
          // For example, assign a default value for the entire row
          for (int i = 0; i < subImageWidth; i++) {
            subImage[j][i] = inputImage[j][i];
          }
        }
      }

      // Calculate the mean color of the sub-image
      png::rgb_pixel meanColor = calculateMeanColor(subImage);
      // Find the closest image from the image map based on the mean color
      string closestImage = findClosestImage(meanColor, imageMap);
      // Load the replacement image
      png::image<png::rgb_pixel> replacementImage(textureFolder + closestImage);

      const int textureRatio = static_cast<int>(
          ceil(replacementImage.get_width() / replacementImage.get_height()));
      const int inputImageRatio = static_cast<int>(
          ceil(inputImage.get_width() / inputImage.get_height()));

      // Flipping the image because of its ratio compared to the input image.
      if (textureRatio < inputImageRatio) {
        replacementImage = flip90DegreesClockwise(replacementImage);
      }

      replacementImage = stretchDown(replacementImage, pixelWidth, pixelHeight);

      if (replacementImage.get_height() == 0 ||
          replacementImage.get_width() == 0) {
        printf("Size can not be 0 ! Error for texture [%s]. It has size (w: "
               "%d, h: %d)\n\tSkipping it.\n",
               closestImage.c_str(), replacementImage.get_width(),
               replacementImage.get_height());
        continue;
      }

      for (int j = 0; j < subImageHeight; j++) {
        // Ensure that the vertical coordinate (y + j) is within the bounds of
        // the output image
        if (y + j < blockEndY) {
          for (int i = 0; i < subImageWidth; i++) {
            // Ensure that the horizontal coordinate (x + i) is within the
            // bounds of the output image
            if (x + i < blockEndX) {
              outputImage[y + j][x + i] = replacementImage[j][i];
            }
          }
        }
      }
    }
  }
}

png::image<png::rgb_pixel> pixelateImageMultithreaded(
    const char *filename,
    const map<png::rgb_pixel, vector<string>, RGBPixelComparator> &imageMap,
    png::image<png::rgb_pixel> &inputImage, int pixelWidth, int pixelHeight,
    int outputWidth, int outputHeight, const char *textureFolder,
    int numThreads, map<string, int> *usageCount) {

  int inputWidth = inputImage.get_width();
  int inputHeight = inputImage.get_height();

  // Calculate the block size for each thread
  int blockWidth = static_cast<int>(inputWidth / numThreads);

  // Create the output image
  png::image<png::rgb_pixel> outputImage(outputWidth, outputHeight);

  //// Vector to store thread objects
  std::vector<std::thread> threads;

  // Create threads
  for (int i = 0; i < numThreads; i++) {
    int startX = i * pixelWidth;
    int endX = (i == numThreads - 1) ? inputWidth : (i + 1) * pixelWidth;
    threads.emplace_back(pixelateBlockRange, filename, std::ref(imageMap),
                         std::ref(inputImage), std::ref(outputImage),
                         pixelWidth, pixelHeight, outputWidth, outputHeight, 0,
                         inputHeight, startX, endX, textureFolder, i);
  }

  printf("Joining threads\n");
  // Join threads
  for (int i = 0; i < numThreads; i++) {
    printf("\tThread %d terminated.\n", i + 1);
    threads[i].join();
  }

  *usageCount = imageUsageCount;

  return outputImage;
}
