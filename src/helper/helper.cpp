#include "helper.h"
#include <cstring>
#include <dirent.h>
#include <iomanip>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

const string OUTPUT_FOLDER_PATH = "output/";
const string IMAGE_PIXELATED_SUFFIX = "_pixelated.png";
const string IMAGE_PIXELATED_BLOCK_USED_SUFFIX = "_blocks.txt";

vector<string> getFilesInFolder(const string &folderPath) {
  vector<string> files;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir(folderPath.c_str())) != nullptr) {
    while ((ent = readdir(dir)) != nullptr) {
      string filename = ent->d_name;
      if (filename != "." && filename != "..") {
        files.push_back(filename);
      }
    }
    closedir(dir);
  } else {
    cerr << "Error opening folder: " << folderPath << endl;
  }
  return files;
}

const char *extractFilename(const char *path) {
  const char *filename = strrchr(path, '/'); // Find last occurrence of '/'
  if (filename == nullptr) {
    // If no '/' found, return original path
    filename = path;
  } else {
    // Return filename part after '/'
    filename += 1;
  }

  // Find extension
  const char *extension = strrchr(filename, '.');
  if (extension != nullptr) {
    // If extension found, truncate at the '.' position
    size_t length = extension - filename;
    char *result = (char *)malloc(length + 1);
    strncpy(result, filename, length);
    result[length] = '\0';
    return result;
  }

  // If no extension found, return filename as is
  return filename;
}

bool endsWith(const string &str, const string &suffix) {
  return str.size() >= suffix.size() &&
         str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

string formatDuration(chrono::milliseconds duration) {
  auto seconds = duration.count() / 1000;
  int hours = seconds / 3600;
  int minutes = (seconds % 3600) / 60;
  seconds = seconds % 60;
  ostringstream oss;
  oss << setw(2) << setfill('0') << hours << ":" << setw(2) << setfill('0')
      << minutes << ":" << setw(2) << setfill('0') << seconds;
  return oss.str();
}

void printOutputMessage(const string &filename, int outputUsage) {
  cout << "Image pixelated and saved as " << OUTPUT_FOLDER_PATH << filename
       << IMAGE_PIXELATED_SUFFIX << endl;
  if (outputUsage)
    cout << "The list of blocks to use in order to make it is saved as "
         << OUTPUT_FOLDER_PATH << filename << IMAGE_PIXELATED_BLOCK_USED_SUFFIX
         << endl;
}

char *appendSlashIfNeeded(char *folderPath) {
  int len = strlen(folderPath);
  if (len > 0 && folderPath[len - 1] != '/') {
    // Append '/' to folderPath
    char *newFolderPath = new char[len + 2]; // +2 for '/' and null terminator
    strcpy(newFolderPath, folderPath);
    strcat(newFolderPath, "/");
    return newFolderPath;
  }
  return folderPath;
}

void printUsage() {
  printf(
      "===================================================================\n"
      "Usage:\n"
      "\t./pixelate [FILENAME_TO_TRANSFORM] [FOLDER_OF_TEXTURES] "
      "[NUMBER_OF_BLOCKS_PER_ROW] [NUMBER_OF_THREADS] [DO_SQUARE_TILING] "
      "[GET_IMAGES_USED] [SCALE_FACTOR]\n"
      "Example:\n\t./pixelate ../pictures/mypicture.png ../pictures/textures "
      "22 4 1 1 1\n"
      "\n\t./pixelate ../pictures/mypicture.png ../pictures/textures "
      "43 12 0 0 2.3\n"
      "\n\t./pixelate ../pictures/mypicture.png ../pictures/textures "
      "128 16 1 0 10\n"
      "\n\t./pixelate ../pictures/mypicture.png ../pictures/textures "
      "64 8 1 0 0.5\n"
      "\n\nNOTE: The image could be stretch by the ratio of the textures.\n"
      "===================================================================\n");
}

void createDirectory(const string &path) {
  if (access(path.c_str(), F_OK) != 0) {
    if (mkdir(path.c_str(), 0777) == -1) {
      printf("Error creating directory: %s.\n", path.c_str());
    }
  }
}

bool isPNG(const string &filename) {
  size_t pos = filename.find_last_of(".");
  if (pos != string::npos) {
    string extension = filename.substr(pos);
    return extension == ".png";
  }
  return false;
}

void convertToPNG(const char *inputFilename) {
  string command = "ffmpeg -i " + string(inputFilename) + " " +
                   string(inputFilename) + ".png -y";
  int result = system(command.c_str());
  if (result != 0) {
    cerr << "Failed to convert " << inputFilename
         << " to PNG format using ffmpeg." << endl;
    exit(1);
  }
}