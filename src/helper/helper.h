#ifndef HELPER_H
#define HELPER_H

#include <chrono>
#include <string>
#include <vector>
using namespace std;

/**
 * Retrieves the list of files in the specified folder.
 * @param folderPath The path to the folder.
 * @return A vector of strings containing the names of files in the folder.
 */
std::vector<std::string> getFilesInFolder(const std::string &folderPath);

/**
 * Extracts the filename from a given path.
 * @param path The path containing the filename.
 * @return The extracted filename.
 */
const char *extractFilename(const char *path);

/**
 * Checks if a string ends with a specific suffix.
 * @param str The string to check.
 * @param suffix The suffix to check for.
 * @return True if the string ends with the specified suffix, false otherwise.
 */
bool endsWith(const string &str, const string &suffix);

/**
 * Formats a duration in milliseconds into a string representation (HH:MM:SS).
 * @param duration The duration to format.
 * @return The formatted duration string.
 */
std::string formatDuration(std::chrono::milliseconds duration);

/**
 * Prints the output message for the pixelation process.
 * @param filename The filename of the pixelated image.
 */
void printOutputMessage(const std::string &filename, int outputUsage);

/**
 * Appends a slash to the folder path if it doesn't already end with one.
 * @param folderPath The folder path to append to.
 * @return The folder path with a trailing slash if needed.
 */
char *appendSlashIfNeeded(char *folderPath);

/**
 * Prints the usage information for the pixelate program.
 */
void printUsage();

void createDirectory(const std::string &path);

bool isPNG(const string &filename);

void convertToPNG(const char *inputFilename);

#endif // HELPER_H
