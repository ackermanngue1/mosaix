# Mosaix

A project where you give an input image with a folder containing textures and the software will make a mosaic of your image based on the given textures.

## Compiling and executing

In order to compile and run this project, you'll need to use the commands :

`cd /src`

`g++ -o pixelate main.cpp helper/helper.cpp image/image.cpp -pthread -lstdc++ -lpng -lm`

`./pixelate [FILENAME_TO_TRANSFORM] [FOLDER_OF_TEXTURES] [NUMBER_OF_BLOCKS_PER_ROW] [NUMBER_OF_THREADS] [DO_SQUARE_TILING] [GET_IMAGES_USED]`

> You will need to have `ffmpeg` installed since if you are using a file that isn't a PNG it will be auto converted into one.
> To download `ffmpeg` you need to use the command `sudo apt install ffmpeg`

### Message libpng warning: iCCP: known incorrect sRGB profile

If you have the following message appearing from your texture folder `libpng warning: iCCP: known incorrect sRGB profile`. You need to go into the folder of your texture and executing the command `mogrify *.png` within the folder. The `mogrify` command comes from `imageMagick`, you can install at with command `sudo apt install imagemagick`.

### Textures

All the textures needs to be `PNG` files. If it is not the case, you can go in the directory of the textures and execute the following command : `for file in *.png; do ffmpeg -i "$file" "${file%.png}.png"; done`

In some cases, the textures are too big and over saturate the RAM. In order to reduce the RAM amount, consider resizing your image with the following command : `for file in *.png; do ffmpeg -i "$file" -vf "scale=NEW_WIDTH:NEW_HEIGHT "${file%.png}_resized.png"; done`

> Replace NEW_WIDTH and NEW_HEIGHT by the desired width and height.

## Dependencies

In order to compile this projet, you'll need to have the library libpng++ installed.

If it is not the case, you can install it by doing :

`sudo apt install libpng++-dev`

## Example

### Executing

These are examples commands from the help usage.

```
======================================================================================================================================
Usage:
	./pixelate [FILENAME_TO_TRANSFORM] [FOLDER_OF_TEXTURES] [NUMBER_OF_BLOCKS_PER_ROW] [NUMBER_OF_THREADS] [DO_SQUARE_TILING] [GET_IMAGES_USED] [SCALE_FACTOR]

Example:
	./pixelate ../pictures/mypicture.png ../pictures/textures 22 4 1 1 1

	./pixelate ../pictures/mypicture.png ../pictures/textures 43 12 0 0 2.3

	./pixelate ../pictures/mypicture.png ../pictures/textures 128 16 1 0 10

	./pixelate ../pictures/mypicture.png ../pictures/textures 64 8 1 0 0.5


NOTE: The image could be stretch by the ratio of the textures.
======================================================================================================================================
```

Executing it with 128 blocks per rows, 5 threads, forced squared tiling (because some textures aren't), get images used writes out in a text file with a 10x upscaling the size of the output image from the input image.

```
clear && g++ -o pixelate main.cpp helper/helper.cpp image/image.cpp -pthread -lstdc++ -lpng -lm && ./pixelate ../example/minecraft.png ../../../Pictures/textures/faithful 128 5 1 1 10
```

### Result

Here is the base image :

![Minecraft logo](./example/minecraft.png)

Here is the pixelated one :

![Pixelated image](./example/minecraft_pixelated.png)

Here is the list of images needed in order to rebuild it :

```
Images used:
acacia_door_bottom.png is used 14 times
acacia_door_top.png is used 252 times
acacia_log_top.png is used 1834 times
acacia_planks.png is used 321 times
azalea_top.png is used 14 times
bamboo_block.png is used 35 times
barrel_side.png is used 1 times
barrel_top.png is used 1 times
barrel_top_open.png is used 47 times
birch_log.png is used 28 times
black_concrete_powder.png is used 35 times
black_shulker_box.png is used 38 times
black_terracotta.png is used 27 times
black_wool.png is used 56 times
blackstone.png is used 142 times
blackstone_top.png is used 1 times
blast_furnace_top.png is used 41 times
brown_concrete_powder.png is used 3015 times
brown_shulker_box.png is used 72 times
brown_terracotta.png is used 16 times
chiseled_deepslate.png is used 4 times
chiseled_stone_bricks.png is used 2 times
coal_block.png is used 483 times
coarse_dirt.png is used 5 times
composter_top.png is used 51 times
cooked_beef.png is used 10 times
cracked_deepslate_tiles.png is used 9 times
cracked_polished_blackstone_bricks.png is used 129 times
cracked_stone_bricks.png is used 9 times
crafting_table_top.png is used 570 times
cyan_terracotta.png is used 18 times
dark_oak_door_bottom.png is used 131 times
dark_oak_door_top.png is used 55 times
dark_oak_planks.png is used 83 times
dark_oak_trapdoor.png is used 35 times
dead_brain_coral_block.png is used 19 times
dead_horn_coral_block.png is used 72 times
deepslate_coal_ore.png is used 11 times
deepslate_gold_ore.png is used 22 times
deepslate_redstone_ore.png is used 3 times
deepslate_tiles.png is used 22 times
deepslate_top.png is used 14 times
diorite.png is used 147 times
dirt.png is used 7 times
dispenser_front.png is used 15 times
dispenser_front_vertical.png is used 10 times
enchanting_table_side.png is used 16 times
exposed_copper.png is used 10 times
furnace_front_on.png is used 6 times
gilded_blackstone.png is used 6 times
gold_ore.png is used 2 times
granite.png is used 7 times
grass_block_side.png is used 3 times
gravel.png is used 58 times
green_concrete_powder.png is used 44 times
green_shulker_box.png is used 791 times
green_terracotta.png is used 83 times
iron_bars.png is used 45 times
iron_block.png is used 166 times
iron_door_bottom.png is used 91 times
iron_trapdoor.png is used 212 times
jungle_log.png is used 9 times
jungle_log_top.png is used 88 times
jungle_trapdoor.png is used 11 times
lectern_base.png is used 820 times
lectern_top.png is used 339 times
light_gray_concrete_powder.png is used 58 times
light_gray_wool.png is used 19 times
lime_concrete_powder.png is used 259 times
lime_wool.png is used 212 times
magma.png is used 331 times
mangrove_log.png is used 23 times
mangrove_log_top.png is used 15 times
melon_side.png is used 1334 times
mossy_stone_bricks.png is used 4 times
mud_bricks.png is used 46 times
mycelium_side.png is used 14 times
nether_bricks.png is used 7 times
netherite_block.png is used 8 times
note_block.png is used 114 times
oak_door_top.png is used 1 times
observer_back.png is used 29 times
observer_front.png is used 6 times
observer_side.png is used 28 times
piston_bottom.png is used 19 times
piston_side.png is used 6 times
piston_top_sticky.png is used 62 times
podzol_side.png is used 24 times
podzol_top.png is used 363 times
polished_andesite.png is used 1 times
polished_blackstone.png is used 5 times
polished_blackstone_bricks.png is used 25 times
polished_deepslate.png is used 16 times
polished_granite.png is used 2 times
powered_rail.png is used 20 times
raw_copper_block.png is used 17 times
redstone_lamp.png is used 1862 times
redstone_ore.png is used 34 times
scaffolding_side.png is used 1 times
scaffolding_top.png is used 20 times
sign.png is used 1 times
smithing_table_bottom.png is used 20 times
smooth_stone_slab_side.png is used 171 times
spruce_door_top.png is used 20 times
spruce_log.png is used 33 times
spruce_log_top.png is used 56 times
spruce_planks.png is used 33 times
stonecutter_bottom.png is used 2 times
stonecutter_top.png is used 14 times
stripped_dark_oak_log_top.png is used 40 times
stripped_spruce_log_top.png is used 2 times
terracotta.png is used 68 times
tnt_side.png is used 3 times
tuff.png is used 107 times
warped_nylium_side.png is used 1 times
wet_sponge.png is used 92 times
wheat_stage7.png is used 1 times
```

```
===================================================================
Image : minecraft
Scale factor  10.000000
Input size (w: 511, h: 512)	Output size (w: 5120, h: 5120)
Texture size is (w: 128, h: 128)	Block size is (w: 40, h: 40)
===================================================================
Thread 4 is computing data from (x: 120, y: 0) to (x: 160, y: 5120)
Thread 1 is computing data from (x: 0, y: 0) to (x: 40, y: 5120)
Thread 5 is computing data from (x: 160, y: 0) to (x: 5120, y: 5120)
Joining threads
Thread 2 is computing data from (x: 40, y: 0) to (x: 80, y: 5120)
	Thread 1 terminated.
Thread 3 is computing data from (x: 80, y: 0) to (x: 120, y: 5120)
	Thread 2 terminated.
	Thread 3 terminated.
	Thread 4 terminated.
	Thread 5 terminated.

Image pixelation and mosaic placement took 00:00:17
Saving the result.....

Writing the image took 00:00:01

Writing the images needed to make the mosaic took 00:00:00
Image pixelated and saved as output/minecraft_pixelated.png
The list of blocks to use in order to make it is saved as output/minecraft_blocks.txt
```

And my configuration is :

```
            .-/+oossssoo+/-.
        `:+ssssssssssssssssss+:`           ----------------------------------
      -+ssssssssssssssssssyyssss+-         OS: Ubuntu 20.04.6 LTS x86_64
    .ossssssssssssssssssdMMMNysssso.       Host: 20TD002MMZ ThinkPad E15 Gen 2
   /ssssssssssshdmmNNmmyNMMMMhssssss/      Kernel: 5.15.0-105-generic
  +ssssssssshmydMMMMMMMNddddyssssssss+	   CPU: 11th Gen Intel i7-1165G7 (8) @ 4.700GHz
 /sssssssshNMMMyhhyyyyhmNMMMNhssssssss/    GPU: NVIDIA 05:00.0 NVIDIA Corporation Device 1f97
.ssssssssdMMMNhsssssssssshNMMMdssssssss.   GPU: Intel Device 9a49
+sssshhhyNMMNyssssssssssssyNMMMysssssss+   Resolution: 1920x1080
ossyNMMMNyMMhsssssssssssssshmmmhssssssso   Memory: 3965MiB / 15688MiB
ossyNMMMNyMMhsssssssssssssshmmmhssssssso
+sssshhhyNMMNyssssssssssssyNMMMysssssss+
.ssssssssdMMMNhsssssssssshNMMMdssssssss.
 /sssssssshNMMMyhhyyyyhdNMMMNhssssssss/
  +sssssssssdmydMMMMMMMMddddyssssssss+
   /ssssssssssshdmNNNNmyNMMMMhssssss/
    .ossssssssssssssssssdMMMNysssso.
      -+sssssssssssssssssyyyssss+-
        `:+ssssssssssssssssss+:`
            .-/+oossssoo+/-.
```
